import os

print('Scanning Application Directories...')

#TOR Check
tor = '/Applications/Tor Browser.app'
brave = '/Applications/Brave Browser.app'

if os.path.exists(tor) or os.path.exists(brave):
    print('Anonymous Browsing Possible: PASS')
    tbscore = 1
    print('Possible via: ')
    if os.path.exists(tor):
        print(tor)
    if os.path.exists(brave):
        print(brave)
    print('')
else:
    print('Anonymous Browsing Possible: FAIL')
    print('Installling TOR or Brave may change this.')


#PRIVACY Browse Check
ww = '/Applications/Waterfox.app'
vivaldi = '/Applications/Vivaldi.app'

chrome = '/Applications/Chrome.app'
ff = '/Applications/FireFox.app'
edge = '/Applications/Microsoft Edge.app'

if os.path.exists(tor) or os.path.exists(brave) or os.path.exists(ww) or os.path.exists(vivaldi):
    print('Private Browsing Possible: PASS')
    bvar1 = 1
    print('Possible via: ')
    if os.path.exists(tor):
        print(tor)
    if os.path.exists(brave):
        print(brave)
    if os.path.exists(ww):
        print(ww)
    if os.path.exists(vivaldi):
        print(vivaldi)        
    print('')
else:
    print('Private Browsing Possible: FAIL')
    print('Installling TOR or Brave may change this.')


pbscore = bvar1

#DOCUMENT EDITOR Check
oo = '/Applications/ONLYOFFICE.app'
pages = '/Applications/pages.app'
numbers = '/Applications/numbers.app'
word = '/Applications/Microsoft Word.app'

if os.path.exists(oo):
    print('Private Document Editing Possible: PASS')
    devar1 = 1
else:
    print('Private Document Editing Possible: FAIL')
    print('Installing OnlyOffice may improve this.')
if os.path.exists(pages) or os.path.exists(numbers):
    
    devar2 = -1

pdescore = devar1+devar2


#DEVELOP Check
vscodium = '/Applications/VSCodium.app'
github = '/Applications/GitHub Desktop.app'
atom = '/Applications/Atom.app'
gitahead = '/Applications/GitAhead.app'

if os.path.exists(vscodium):
    print('Private Development Possible: PASS')
    dvar1 = 1
else:
    print('Private Development Possible: FAIL')
    print('Installing VSCodium may improve this.')
    dvar1 = 0

if os.path.exists(github) or os.path.exists(atom):
    
    dvar2 = -1

if os.path.exists(gitahead):
    
    dvar3 = 1
    
dscore = dvar1+dvar2+dvar3

#NETWORK Check
protonvpn = '/Applications/ProtonVPN.app'

if os.path.exists(protonvpn):
    print('Private/Secure Network Possible: PASS')
    nvar1 = 1
else:
    print('Private/Secure Network Possible: FAIL')
    print('Installing ProtonVPN may improve this.')
    nvar1 = 0

network = nvar1

#COMS Check
session = '/Applications/Session.app'
signal = '/Applications/Signal.app'
telegram = '/Applications/Telegram.app'
messenger = '/Applications/Messenger.app'
zoom = '/Applications/Zoom.app'

if os.path.exists(session) or os.path.exists(signal) or os.path.exists(telegram):
    print('Secure Comunication Possible: PASS')
    cvar1 = 1
else:
    print('Secure Comunication Possible: FAIL')
    cvar1 = -1

if os.path.exists(session):
    print('TOR Comunication Possible: PASS')
    cvar2 = 1
else:
    print('TOR Comunication Possible: FAIL')
    cvar2 = 0

if os.path.exists(session) or os.path.exists(signal):
    print('Censorship Resistant Comunication Possible: PASS')
    cvar3 = 1
else:
    print('Censorship Resistant Comunication Possible: FAIL')
    cvar3 = 0

if os.path.exists(messenger) or os.path.exists(zoom):
    cvar4 = -1
else:
    cvar4 = 0

network = cvar1+cvar2+cvar3+cvar4

if network <= 2 :
    print('Installing Session may improve this.')


#Issues

if os.path.exists(chrome) or os.path.exists(edge) or os.path.exists(ff):
    print('There is an inscure/non-private browser on this system.')
else:
    print()

if os.path.exists(pages) or os.path.exists(numbers) or os.path.exists(word):
    print('There is an inscure/non-private document editor on this system.')
else:
    print()

if os.path.exists(github) or os.path.exists(atom):
    print('There is an inscure/non-private development app on this system.')
else:
    print()

#Total Privacy Score
tps = tbscore+pbscore+pdescore+dscore+network
print('')
print('PRIVACY GRADE: ')

if tps >= 5 :
    print('PASS')
else:
    print('FAIL')
